#!/usr/bin/python3

import webapp
import urllib.parse


class ShortenApp(webapp.webApp):
    urls = {
    }

    def parse(self, request):
        method = request.split(' ', 2)[0]
        resource = request.split(' ', 2)[1]
        body = request.split('\n')[-1]
        return method, resource, body

    def process(self, petition):

        method, resource, body = petition

        form = """
            <form action="" method="POST">
                    <p>Introduce the url: <input type="text" name="url"/>
                    <p>Shortened url: <input type="text" name="short"/>
                    <p><input type="submit" value="Shorten url"/>
            </form>
        """
        ErrorCode = "404 Not Found"
        ErrorBody = "<html><body>Not Found. Click here to return to the main page: " \
                    '<a href="/">Return</a>'

        if method == "GET":
            if resource in self.urls.keys():
                httpCode = "308 Permanent Redirect"
                htmlBody = "<html><body>You're going to be redirected to: " \
                           + '<a href="' + self.urls[resource] + ' ">"' + self.urls[resource] + '"</a><br>' \
                           + "<meta http-equiv='refresh' content='3;URL=" + self.urls[resource] + "'>" + "Please wait."

            elif resource == "/":
                httpCode = "200 OK"
                htmlBody = "<html><body>Hi! This app allows you to shorten any link." \
                           "<p>Use this form if you want to shorten an url.</p>" + form \
                           + "<p>Shortened urls: </p>" + str(self.urls)

            else:
                httpCode = ErrorCode
                htmlBody = ErrorBody
            htmlBody += "</body></html>"
            return httpCode, htmlBody

        else:  # When method == POST
            url = urllib.parse.unquote(body.split("&")[0].split("=")[1])
            short = '/' + body.split('&')[1].split('=')[1]

            if not url.startswith('http://') and not url.startswith('https://'):
                url = "https://" + url

            if url != "" and short != "/":
                self.urls[short] = url
                httpCode = "200 OK"
                htmlBody = "<html><body>Long url: " \
                           + '<a href="' + self.urls[short] + ' ">"' + self.urls[short] + '"</a><br>' \
                           + "<p>Shortened url: "'<a href="' + self.urls[short] + ' ">"' + short + '"</a></p>'

            else:
                httpCode = ErrorCode
                htmlBody = ErrorBody
            htmlBody += "</body></html>"
            return httpCode, htmlBody


if __name__ == "__main__":
    testWebApp = ShortenApp("localhost", 1234)
